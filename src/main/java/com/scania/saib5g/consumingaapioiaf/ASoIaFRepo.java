package com.scania.saib5g.consumingaapioiaf;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

@Component
public class ASoIaFRepo {

    public List<String> getAllTitles() throws IOException {
        URL url = new URL("https://anapioficeandfire.com/api/houses/378");
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");

        if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
            InputStreamReader inputStreamReader = new InputStreamReader(connection.getInputStream());
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            StringBuffer responseContent = new StringBuffer();

            String line;
            while ((line = bufferedReader.readLine()) != null) {
                responseContent.append(line);
            }
            bufferedReader.close();

            JSONObject jsonObject = new JSONObject(responseContent.toString());
            List<String> titles = new ArrayList<>();
            JSONArray jsonArray = jsonObject.getJSONArray("titles");
            for (Object obj : jsonArray) {
                titles.add(obj.toString());
            }

            return titles;
        }
        return null;
    }
}

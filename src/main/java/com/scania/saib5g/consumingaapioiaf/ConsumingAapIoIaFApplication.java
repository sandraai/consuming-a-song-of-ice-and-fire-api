package com.scania.saib5g.consumingaapioiaf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConsumingAapIoIaFApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConsumingAapIoIaFApplication.class, args);
    }

}

package com.scania.saib5g.consumingaapioiaf;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;

@RestController
@RequestMapping("/api/targaryen")
public class ASoIaFController {

    @Autowired
    private ASoIaFRepo aSoIaFRepo;

    @GetMapping
    public ResponseEntity<List<String>> getAllTitles () throws IOException {
        return new ResponseEntity<>(aSoIaFRepo.getAllTitles(), HttpStatus.OK);
    }
}
